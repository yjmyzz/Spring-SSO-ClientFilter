package infosky.ckg.sso.constant;

public final class SsoConsts {

	public static final String TOKEN_NAME = "_token";

	public static final String RETURN_URL_NAME = "returnUrl";

}
