package infosky.ckg.utils.web;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

public class HttpUtil {

	static Logger log = Logger.getLogger(HttpUtil.class);

	public static String get(String url, String proxyHost, Integer proxyPort) {
		String response = null;
		HttpClient client = new DefaultHttpClient();

		if (!StringUtils.isEmpty(proxyHost) && proxyHost != null) {

			HttpHost host = new HttpHost(proxyHost, proxyPort);
			client.getParams()
					.setParameter(ConnRoutePNames.DEFAULT_PROXY, host);

		}
		HttpGet httpGet = new HttpGet(url);
		try {
			HttpResponse httpResponse = client.execute(httpGet);
			log.debug(httpResponse.getStatusLine().getStatusCode() + " " + url);
			response = EntityUtils.toString(httpResponse.getEntity());
			log.debug(response);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("调用服务失败，服务地址：" + url + "，异常类型："
					+ e.getClass() + "，错误原因：" + e.getMessage());
		}
		return response;

	}

	public static String get(String url) {
		return get(url, null, null);

	}

}
