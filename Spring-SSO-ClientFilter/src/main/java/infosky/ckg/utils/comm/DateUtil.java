package infosky.ckg.utils.comm;

import java.util.Calendar;
import java.util.Date;

public final class DateUtil {

	public static Date addHours(Date src, int addHours) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(src);
		calendar.add(Calendar.HOUR, addHours);
		return calendar.getTime();

	}
}
